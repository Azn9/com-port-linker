using Com0Com.CSharp;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

/**
 * This file was written by Axel Joly
 * This software was created for Lycée Paul Constans
 */
namespace COM_Port_Linker
{
    public partial class Form1 : Form
    {
        private static readonly Com0ComSetupCFacade SetupCFacade = new Com0ComSetupCFacade();

        private List<string> _availablePort;
        private static string _selectedPort1;
        private static string _selectedPort2;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SetupCFacade.GetCrossoverPortPairs().ToList()
                .ForEach(pp => SetupCFacade.DeletePortPair(pp.PairNumber));
        }

        private void UpdateOpenedPorts()
        {
            var preExistingPortPairs = SetupCFacade.GetCrossoverPortPairs();
            richTextBox1.Clear();
            var crossoverPortPairs = preExistingPortPairs as CrossoverPortPair[] ?? preExistingPortPairs.ToArray();
            crossoverPortPairs.ToList().ForEach(pp =>
                richTextBox1.AppendText($"{pp.PairNumber} - {pp.PortNameA} <-> {pp.PortNameB}" +
                                              Environment.NewLine));

            comboBox3.BeginUpdate();
            comboBox3.Items.Clear();
            crossoverPortPairs.ToList().ForEach(pp => comboBox3.Items.Add($"{pp.PairNumber}"));
            comboBox3.EndUpdate();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var ports = SerialPort.GetPortNames();
            _availablePort = Enumerable.Range(1, 254)
                .Select(i => i < 10 ? $"00{i}" : i < 100 ? $"0{i}" : $"{i}").Select(i => $"COM{i}")
                .Where(s => !ports.Contains(s)).ToList();

            _availablePort?.ToList().ForEach(s =>
            {
                comboBox1.Items.Add(s);
                comboBox2.Items.Add(s);
            });

            SetupCFacade.GetCrossoverPortPairs().ToList()
                .ForEach(pp => SetupCFacade.DeletePortPair(pp.PairNumber));

            UpdateOpenedPorts();
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedPort = comboBox1.SelectedItem.ToString();

            comboBox2.Items.Remove(selectedPort);

            if (_selectedPort1 == selectedPort) return;

            if (_selectedPort1 != null)
                comboBox2.Items.Add(_selectedPort1);
            _selectedPort1 = selectedPort;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (_selectedPort1 == null || _selectedPort2 == null)
            {
                MessageBox.Show("Veuillez sélectionner deux ports à lier !");
                return;
            }

            var thread = new Thread(ThreadWork.Work);
            thread.Start();

            var selected1 = $"COM{int.Parse(_selectedPort1.Split('M')[1])}";
            var selected2 = $"COM{int.Parse(_selectedPort2.Split('M')[1])}";

            comboBox1.Items.Remove(_selectedPort1);
            comboBox1.Items.Remove(_selectedPort2);
            comboBox2.Items.Remove(_selectedPort1);
            comboBox2.Items.Remove(_selectedPort2);

            _selectedPort1 = null;
            _selectedPort2 = null;

            var pp = SetupCFacade.CreatePortPair(selected1, selected2);
            UpdateOpenedPorts();

            MessageBox.Show(
                "Pont de port COM créé !\n\nAttention: les ports COM ne seront utilisables qu'à la fin de l'installation des pilotes de périphériques !\nVous pouvez accélérer le processus en cliquant sur le message \"Ne pas obtenir de logiciel pilote depuis Windows Update\" dans la fenêtre d'installation des pilotes.");

            thread.Abort();
        }

        private class ThreadWork
        {
            public static void Work()
            {
                MessageBox.Show(
                    "Création du pont de port COM, veuillez patienter...\nCela peut durer 2min maximum à cause de l'installation des pilotes de périphériques.",
                    "Status");
            }
        }

        private void Label3_Click(object sender, EventArgs e)
        {
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            if (comboBox3.SelectedItem == null || comboBox3.SelectedItem.ToString() == "")
            {
                MessageBox.Show("Veuillez sélectionner l'ID de pont à supprimer !");
                return;
            }

            var thread = new Thread(ThreadWork2.Work);
            thread.Start();

            SetupCFacade.DeletePortPair(int.Parse(comboBox3.SelectedItem.ToString()));

            thread.Abort();
            UpdateOpenedPorts();

            MessageBox.Show("Pont de port COM supprimé !");
        }

        private class ThreadWork2
        {
            public static void Work()
            {
                MessageBox.Show("Suppression du pont de port COM, veuillez patienter...", "Status");
            }
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedPort = comboBox2.SelectedItem.ToString();

            comboBox1.Items.Remove(selectedPort);

            if (_selectedPort2 == selectedPort) return;

            if (_selectedPort2 != null)
                comboBox1.Items.Add(_selectedPort2);
            _selectedPort2 = selectedPort;
        }
    }
}