﻿using System;
using System.Windows.Forms;

/**
 * This file was written by Axel Joly
 * This software was created for Lycée Paul Constans
 */
namespace COM_Port_Linker
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}