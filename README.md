# COM Port Linker

### Summary :
This project is used to create a link between two virtual COM ports.
It uses [com0com](http://com0com.sourceforge.net/) to create virtual ports and [NSIS](https://nsis.sourceforge.io/Main_Page) to create the installer.

This software was created for Lycée Paul Constans by Axel Joly.

### Build :
Open the .sln file using Visual Studio 2017+ or JetBrains Rider.
Compile using [Com0Com.CSharp](https://www.nuget.org/packages/Com0Com.CSharp) nuget dependency.

### Install :
1. Copy the contents of the "Installer" folder into the `C:\INSTALLER` folder.
2. Download the standalone dotNetFx setup [here](https://www.microsoft.com/fr-fr/download/details.aspx?id=48137) for dotnet 4.5+, rename it `dotNetFx40setup.exe` and put it on the `C:\INSTALL` folder.
3. Install [NSIS](https://nsis.sourceforge.io/Main_Page) with the [UAC Plugin](https://nsis.sourceforge.io/UAC_plug-in) and the [StdUtils plugin](https://nsis.sourceforge.io/StdUtils_plug-in).
4. Run the `installer.nsis` file.