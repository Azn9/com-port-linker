;--------------------------------
;Includes

  !include "MUI2.nsh"
  !include UAC.nsh
  !include 'StdUtils.nsh'
  !include LogicLib.nsh
  !include x64.nsh

;--------------------------------
;General

  ;Name and file
  Name "COM Port Linker"
  OutFile "COM Port Linker Installer.exe"

  Icon "C:\INSTALLER\icon.ico"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\COM Port Linker"

  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\COM Port Linker" ""

  ShowInstDetails show
  RequestExecutionLevel user

;--------------------------------
;Variables

  Var StartMenuFolder

;--------------------------------
;Interface Settings

  !define MUI_ICON "C:\INSTALLER\icon.ico"
  !define MUI_ABORTWARNING
  !define MUI_FINISHPAGE_RUN "$INSTDIR\CPL.exe"
  !define MUI_FINISHPAGE_RUN_TEXT "Lancer COM Port Linker"

;--------------------------------
;X64

!macro xsq
x64:
Section
${If} ${RunningX64}
${Else}
  MessageBox mb_YesNo|mb_IconExclamation|mb_TopMost|mb_SetForeground "This ${thing} requires 64x OS" /SD IDNO IDYES x64 IDNO 0
  Quit
${EndIf}  
SectionEnd
!macroend

;--------------------------------
;UAC
!macro Init thing
uac_tryagain:
!insertmacro UAC_RunElevated
${Switch} $0
${Case} 0
  ${IfThen} $1 = 1 ${|} Quit ${|} ;we are the outer process, the inner process has done its work, we are done
  ${IfThen} $3 <> 0 ${|} ${Break} ${|} ;we are admin, let the show go on
  ${If} $1 = 3 ;RunAs completed successfully, but with a non-admin user
    MessageBox mb_YesNo|mb_IconExclamation|mb_TopMost|mb_SetForeground "This ${thing} requires admin privileges, try again" /SD IDNO IDYES uac_tryagain IDNO 0
  ${EndIf}
  ;fall-through and die
${Case} 1223
  MessageBox mb_IconStop|mb_TopMost|mb_SetForeground "This ${thing} requires admin privileges, aborting!"
  Quit
${Case} 1062
  MessageBox mb_IconStop|mb_TopMost|mb_SetForeground "Logon service not running, aborting!"
  Quit
${Default}
  MessageBox mb_IconStop|mb_TopMost|mb_SetForeground "Unable to elevate, error $0"
  Quit
${EndSwitch}
 
SetShellVarContext all
!macroend

Function .onInit
!insertmacro Init "installer"
FunctionEnd
 
Function un.onInit
!insertmacro Init "uninstaller"
FunctionEnd

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "C:\INSTALLER\Licence.txt"
  !insertmacro MUI_PAGE_DIRECTORY

  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\COM Port Linker" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "COM Port Linker"

  !define MUI_FINISHPAGE_NOAUTOCLOSE
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "French"

;--------------------------------
;Installer Sections

!define NETVersion "4.0.30319"
!define NETInstaller "dotNetFx40setup.exe"
Section "Dummy Section" SecDummy
  SetOutPath "$INSTDIR"

  File "CPL.exe"
  File "Com0Com.CSharp.dll"

  ;Store installation folder
  WriteRegStr HKCU "Software\COM Port Linker" "" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Com Port Linker.lnk" "$INSTDIR\CPL.exe"
    CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_END

  File /oname=$TEMP\com0com.exe com0com.exe
  ${StdUtils.ExecShellWaitEx} $0 $1 "$TEMP\com0com.exe" "open" "/S"

  DetailPrint "Installing com0com..."
  StrCmp $0 "error" ExecFailed ;check if process failed to create
  StrCmp $0 "no_wait" WaitNotPossible ;check if process can be waited for - always check this!
  StrCmp $0 "ok" WaitForProc ;make sure process was created successfully
  Abort

  WaitForProc:
  ${StdUtils.WaitForProcEx} $2 $1
  Goto WaitDone

  ExecFailed:
  DetailPrint "Failed to create process (error code: $1)"
  Goto WaitDone

  WaitNotPossible:
  Goto WaitDone

  WaitDone:

  DetailPrint "Com0com successfully installed !"

  IfFileExists "$WINDIR\Microsoft.NET\Framework\v${NETVersion}" NETFrameworkInstalled 0
  File /oname=$TEMP\${NETInstaller} ${NETInstaller}
 
  DetailPrint "Starting Microsoft .NET Framework v${NETVersion} Setup..."
  ExecWait "$TEMP\${NETInstaller}"
  Return
 
  NETFrameworkInstalled:
  DetailPrint "Microsoft .NET Framework is already installed!"
SectionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  nsExec::Exec '"$PROGRAMFILES\com0com\uninstall.exe"'
  Delete "$INSTDIR\*"
  RMDir "$INSTDIR"

  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
  
  Delete "$SMPROGRAMS\$StartMenuFolder\COM Port Linker.lnk"  
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"

  DeleteRegKey /ifempty HKCU "Software\COM Port Linker"

SectionEnd
